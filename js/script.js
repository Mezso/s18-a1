console.log("Hello World")

let Trainer1 = Object();
Trainer1.name = "Carlo";
Trainer1.age = 25;
Trainer1.height = "169.5cm"

function Creation(pname){
	this.PokeName = pname
	this.health = 100;
	this.tackle = function(target){
		console.log(`${this.PokeName} attacked ${target.PokeName}`);
		target.health -= 10;
		console.log(`${this.PokeName}'s health is now ${target.health}`);
	}
}
let Charmander = new Creation("Charmander")
let Pigeot = new Creation("Pigeot")
console.log(Trainer1,Charmander,Pigeot)
Charmander.tackle(Pigeot)
Charmander.tackle(Pigeot)
Pigeot.tackle(Charmander)
Pigeot.tackle(Charmander)
Pigeot.tackle(Charmander)
Charmander.tackle(Pigeot)
Charmander.tackle(Pigeot)